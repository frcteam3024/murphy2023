// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.GyroSubsystem;

public class GyroReset extends CommandBase {
  private final GyroSubsystem gyroSubsystem;
  /** Creates a new GyroReset. */
  public GyroReset(GyroSubsystem gyroSubsystem) {
    this.gyroSubsystem = gyroSubsystem;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(gyroSubsystem);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    gyroSubsystem.resetGyro();
  }

}
