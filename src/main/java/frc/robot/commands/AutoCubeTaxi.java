// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.subsystems.SwerveSubsystem;
import static frc.robot.Constants.AutoTaxiConstants.*;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/stable/docs/software/commandbased/convenience-features.html
public class AutoCubeTaxi extends SequentialCommandGroup {
  
  /** Creates a new AutoTaxiTest. */
  public AutoCubeTaxi(
    SwerveSubsystem swerveSubsystem
  ) {
    // Add your commands in the addCommands() call, e.g.
    // addCommands(new FooCommand(), new BarCommand());
    addCommands(
      new TaxiForDuration(swerveSubsystem, BACK_UP, BACK_UP_DURATION),
      new TaxiForDuration(swerveSubsystem, EXIT_COMMUNITY, EXIT_COMMUNITY_DURATION)
    );
  }
}
