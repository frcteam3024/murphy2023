package frc.robot.commands;

import static frc.robot.Constants.ArmConstants.*;
import static java.lang.Math.abs;

import java.util.function.Supplier;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ArmSubsystem;

public class ArmCommand extends CommandBase {

  private ArmSubsystem armSubsystem;
  private Supplier<Double> leftAxisFunction;

  public ArmCommand(
      ArmSubsystem armSubsystem,
      Supplier<Double> leftAxisFunction
    ) {
    this.armSubsystem = armSubsystem;
    this.leftAxisFunction = leftAxisFunction;
    addRequirements(armSubsystem);
  }

  @Override
  public void initialize() {
    armSubsystem.brakeMode();
  }

  @Override
  public void execute() {
    double driverLeftAxis = leftAxisFunction.get();
    double armMotorOutput = driverLeftAxis;
    if (abs(armMotorOutput) < MIN_MOTOR_OUTPUT) armMotorOutput = NO_OUTPUT;

    if (ARM_OUTPUT_DEBUG_MODE) SmartDashboard.putNumber("armMotorOut", armMotorOutput);
    armSubsystem.setArmMotorOutput(armMotorOutput);
  }

  @Override
  public void end(boolean interrupted) {
    armSubsystem.setArmMotorOutput(NO_OUTPUT);
    armSubsystem.coastMode();
  }

  @Override
  public boolean isFinished() {
    return false;
  }
}
