// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.GyroSubsystem;
import frc.robot.subsystems.SwerveSubsystem;
import static frc.robot.Constants.AutoChargeConstants.*;
import static frc.robot.Constants.TimerConstants.*;

public class StationPulse extends CommandBase {
  private final SwerveSubsystem swerveSubsystem;
  private final GyroSubsystem gyroSubsystem;
  private final Timer pulseTimer;
  private final Timer balanceTimer;
  private boolean pulsing;
  private boolean tooFar;

  /** Creates a new StationPulse. */
  public StationPulse(
      SwerveSubsystem swerveSubsystem,
      GyroSubsystem gyroSubsystem
  ) {
    this.swerveSubsystem = swerveSubsystem;
    this.gyroSubsystem = gyroSubsystem;
    this.pulseTimer   = new Timer();
    this.balanceTimer = new Timer();
    this.pulsing = true;
    this.tooFar = false;

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(swerveSubsystem, gyroSubsystem);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    pulseTimer.start();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    // initialize a timer once flat
    if (gyroSubsystem.isFlat()) {
      if (!balanceTimer.hasElapsed(MIN_TIMER_DURATION)) {
        balanceTimer.start();
      }
    }
    else /* not flat */ {
      balanceTimer.reset();
    }

    // set tooFar if tilted downwards, reset otherwise
    if (gyroSubsystem.tiltedDown()) {
      tooFar = true;
    }
    else if (gyroSubsystem.tiltedUp()) {
      tooFar = false;
    }

    // pulse while tilted upwards
    if (pulsing) {
      ChassisSpeeds pulseMovement = (!tooFar ? PULSE_FORWARD : PULSE_BACKWARD);
      setMovementForDuration(pulseMovement, COAST_DURATION);
    }
    else /* coast*/ { 
      setMovementForDuration(COAST, COAST_DURATION);
    }
  }

  private void setMovementForDuration(ChassisSpeeds desiredMovement, double duration) {
    if (pulseTimer.hasElapsed(duration)) {
      swerveSubsystem.setModules(desiredMovement);
    }
    else {
      pulseTimer.reset();
      pulsing = !pulsing;
      toggleIdleMode();
    }
    
  }

  private void toggleIdleMode() {
    if (pulsing) {
      swerveSubsystem.brakeMode();
    }
    else {
      swerveSubsystem.coastMode();
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    swerveSubsystem.brakeMode();
    swerveSubsystem.setModules(STILL);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return balanceTimer.hasElapsed(BALANCE_TIMEOUT);
  }
}
