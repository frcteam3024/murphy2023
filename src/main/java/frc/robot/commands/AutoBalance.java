// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.subsystems.GyroSubsystem;
import frc.robot.subsystems.SwerveSubsystem;

import static frc.robot.Constants.AutoChargeConstants.*;

public class AutoBalance extends SequentialCommandGroup {

  /** Creates a new AutoBalance. */
  public AutoBalance(
      SwerveSubsystem swerveSubsystem,
      GyroSubsystem   gyroSubsystem
  ) {
    addCommands(
      new TaxiToStation(swerveSubsystem, gyroSubsystem, DRIVE_TO_STATION),
      new StationPulse(swerveSubsystem, gyroSubsystem)
    );
  }
}
