// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.SwerveSubsystem;

public class TaxiForDuration extends CommandBase {
  private final SwerveSubsystem swerveSubsystem;
  private final ChassisSpeeds desiredMovement;
  private final double duration;
  private final Timer timer;

  /** Creates a new TaxiCommand. */
  public TaxiForDuration(
      SwerveSubsystem swerveSubsystem,
      ChassisSpeeds desiredMovement,
      double duration
  ) {

    this.swerveSubsystem = swerveSubsystem;
    this.desiredMovement = desiredMovement;
    this.duration = duration;
    this.timer = new Timer();

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(swerveSubsystem);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    swerveSubsystem.brakeMode();
    timer.start();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    swerveSubsystem.setModules(desiredMovement);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    timer.stop();
    timer.reset();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return timer.get() > duration;
  }
}
