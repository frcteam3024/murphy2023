// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import static frc.robot.Constants.DriveConstants.*;
import static frc.robot.Constants.OIConstants.*;
import static java.lang.Math.*;

// import java.util.Arrays;
// import java.util.Collections;
// import java.util.List;
import java.util.function.Supplier;
// import java.util.stream.Collectors;

import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.GyroSubsystem;
import frc.robot.subsystems.SwerveSubsystem;

public class SwerveDriveCommand extends CommandBase {
  /** Creates a new SwerveJoystickCommand. */
    private final SwerveSubsystem   swerveSubsystem;
    private final GyroSubsystem     gyroSubsystem;
    private final Supplier<Double>  xSpeedFunction;
    private final Supplier<Double>  ySpeedFunction;
    private final Supplier<Double>  turnSpeedFunction;
    private final Supplier<Boolean> robotOrientedFunction;

    // TODO rate limiters, max speed

  public SwerveDriveCommand(
      SwerveSubsystem swerveSubsystem,
      GyroSubsystem gyroSubsystem,
      Supplier<Double> xSpeedFunction,
      Supplier<Double> ySpeedFunction,
      Supplier<Double> turnSpeedFunction,
      Supplier<Boolean> robotOrientedFunction
    ) {
    this.swerveSubsystem = swerveSubsystem;
    this.gyroSubsystem = gyroSubsystem;
    this.xSpeedFunction = xSpeedFunction;
    this.ySpeedFunction = ySpeedFunction;
    this.turnSpeedFunction = turnSpeedFunction;
    this.robotOrientedFunction = robotOrientedFunction;
    addRequirements(swerveSubsystem);
  }

  @Override
  public void initialize() {
    swerveSubsystem.brakeMode();
  }
  
  @Override
  public void execute() {
    // get real-time joystick inputs
    double xSpeed = xSpeedFunction.get();
    double ySpeed = ySpeedFunction.get();
    double turnSpeed = turnSpeedFunction.get();

    // apply deadband
    if (abs(xSpeed)    < AXIS_DEADBAND) xSpeed = NO_OUTPUT;
    if (abs(ySpeed)    < AXIS_DEADBAND) ySpeed = NO_OUTPUT;
    if (abs(turnSpeed) < TURN_DEADBAND) turnSpeed = NO_OUTPUT;

    // cube inputs for better sensitivity
    xSpeed = pow(xSpeed, 3);
    ySpeed = pow(ySpeed, 3);
    turnSpeed = pow(turnSpeed, 3);

    if (OI_DEBUG_MODE) {
      SmartDashboard.putNumber("raw x", xSpeed);
      SmartDashboard.putNumber("raw y", ySpeed);
      SmartDashboard.putNumber("raw turn", turnSpeed);
    }

    // convert to meters per second
    xSpeed *= MAX_DRIVE_SPEED;
    ySpeed *= MAX_DRIVE_SPEED;
    turnSpeed *= MAX_TURN_SPEED_RAD;

    // construct desired chassis speeds
    ChassisSpeeds chassisSpeeds;
    if (robotOrientedFunction.get().equals(Boolean.TRUE)) {
      // relative to robot
      chassisSpeeds = new ChassisSpeeds(ySpeed, xSpeed, turnSpeed);
    } else {
      // relative to field
      chassisSpeeds = ChassisSpeeds.fromFieldRelativeSpeeds(
          ySpeed, xSpeed, turnSpeed, gyroSubsystem.getGyroRotation2d());
    }

    if (OI_DEBUG_MODE) SmartDashboard.putString("chassisSpeeds", chassisSpeeds.toString());

    // output module states to each wheel
    swerveSubsystem.setModules(chassisSpeeds);
  }

  @Override
  public void end(boolean interrupted) {
    swerveSubsystem.stopMotors();
    swerveSubsystem.coastMode();
  }
  
  @Override
  public boolean isFinished() {
    return false;
  }
}
