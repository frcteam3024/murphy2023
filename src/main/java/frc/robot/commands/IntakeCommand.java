package frc.robot.commands;

import static frc.robot.Constants.IntakeConstants.*;
import static java.lang.Math.abs;

import java.util.function.Supplier;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.IntakeSubsystem;

public class IntakeCommand extends CommandBase {

  private IntakeSubsystem intakeSubsystem;
  private Supplier<Double> speedFunction;

  public IntakeCommand(
      IntakeSubsystem driveSubsystem,
      Supplier<Double> speedFunction
    ) {
    this.intakeSubsystem = driveSubsystem;
    this.speedFunction = speedFunction;
    addRequirements(intakeSubsystem);
  }

  @Override
  public void initialize() {
    intakeSubsystem.brakeMode();
  }

  @Override
  public void execute() {
    double driverInput = speedFunction.get();
    double intakeOutput = driverInput * MAX_INTAKE_SPEED;

    if (abs(intakeOutput) < MIN_INTAKE_SPEED) intakeOutput = NO_OUTPUT;
    intakeSubsystem.setOutput(intakeOutput);
  }

  @Override
  public void end(boolean interrupted) {
    intakeSubsystem.setOutput(NO_OUTPUT);
    intakeSubsystem.coastMode();
  }

  @Override
  public boolean isFinished() {
    return false;
  }
}
