// // Copyright (c) FIRST and other WPILib contributors.
// // Open Source Software; you can modify and/or share it under the terms of
// // the WPILib BSD license file in the root directory of this project.
// 
// package frc.robot.commands;
// 
// import edu.wpi.first.wpilibj.Timer;
// import edu.wpi.first.wpilibj2.command.CommandBase;
// import frc.robot.subsystems.ArmSubsystem;
// import frc.robot.subsystems.IntakeSubsystem;
// import static frc.robot.Constants.AutoShelfPlaceConstants.*;
// import static frc.robot.Constants.GyroConstants.*;
// import static frc.robot.Constants.IntakeConstants.*;
// import static java.lang.Math.*;
// 
// public class AutoShelfPlace extends CommandBase {
// 
//   private final ArmSubsystem armSubsystem;
//   private final IntakeSubsystem intakeSubsystem;
//   private final Timer timer;
// 
//   /** Creates a new AutoShelfPlace. */
//   public AutoShelfPlace(
//     ArmSubsystem armSubsystem,
//     IntakeSubsystem intakeSubsystem
//   ) {
//     this.armSubsystem = armSubsystem;
//     this.intakeSubsystem = intakeSubsystem;
//     this.timer = new Timer();
//     // Use addRequirements() here to declare subsystem dependencies.
//     addRequirements(
//       armSubsystem,
//       intakeSubsystem
//     );
//   }
// 
//   // Called when the command is initially scheduled.
//   @Override
//   public void initialize() {}
// 
//   // Called every time the scheduler runs while the command is scheduled.
//   @Override
//   public void execute() {
//     armSubsystem.setArmMotorToAngle(SHELF_INCLINE_ANGLE);
//     double currentAngle = armSubsystem.getArmAngle();
//     if (abs(currentAngle - SHELF_INCLINE_ANGLE) < GYRO_TOLERANCE) {
//       if (!timer.hasElapsed(0)) {
//         timer.start();
//       }
//       intakeSubsystem.setOutput(MAX_INTAKE_SPEED);
//     }
//   }
// 
//   // Called once the command ends or is interrupted.
//   @Override
//   public void end(boolean interrupted) {
//     double zeroSpeed = 0;
//     intakeSubsystem.setOutput(zeroSpeed);
//   }
// 
//   // Returns true when the command should end.
//   @Override
//   public boolean isFinished() {
//     return timer.hasElapsed(OUTTAKE_DURATION);
//   }
// }
// 