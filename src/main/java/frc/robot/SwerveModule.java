// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import static frc.robot.Constants.ModuleConstants.*;
import static frc.robot.Constants.DriveConstants.*;
import static java.lang.Math.*;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.math.controller.PIDController;
// import edu.wpi.first.math.controller.ProfiledPIDController;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.kinematics.SwerveModuleState;
// import edu.wpi.first.math.trajectory.TrapezoidProfile;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.AnalogEncoder;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class SwerveModule {

    private final CANSparkMax driveMotor;
    private final CANSparkMax turnMotor;
    private final AnalogEncoder turnEncoder;
    // TODO use turnPID instead of doing calculations manually
    private final PIDController turnPID;
    private final double encoderOffsetDeg;
    private final boolean turnEncoderReversed;
    private final int channel;

    // private double encoderAngle; // [0,1]
    // private final double encoderOutScalar = 0.1;

    /** contains drivae and turn motors and all methods to directly interact with them */
    public SwerveModule(
        int driveMotorID,
        int turnMotorID,
        int encoderID,
        double encoderOffsetDeg,
        boolean driveMotorReversed,
        boolean turnMotorReversed,
        boolean turnEncoderReversed
      ) {

      this.driveMotor = new CANSparkMax(driveMotorID, MotorType.kBrushless);
      this.driveMotor.setInverted(driveMotorReversed);

      this.turnMotor = new CANSparkMax(turnMotorID, MotorType.kBrushless);
      this.turnMotor.setInverted(turnMotorReversed);

      this.turnEncoder = new AnalogEncoder(encoderID);
      // this.turnEncoder.setDistancePerRotation(2.0 * Math.PI);
      this.turnEncoderReversed = turnEncoderReversed;
      this.encoderOffsetDeg = encoderOffsetDeg;
      // this.encoderAngle = encoderOffsetDeg;

      this.turnPID = new PIDController(
        KP_TURN, KI_TURN, KD_TURN
        // new TrapezoidProfile.Constraints(
        //   MAX_TURN_SPEED,
        //   MAX_TURN_ACCEL
        // )
      );

      this.turnPID.enableContinuousInput(-180, 180);
      this.channel = turnEncoder.getChannel();
    
      resetEncoders();
    }

    /**
     * @return angle in degrees of the turn motor
     */
    public double getTurnDeg() {
      // final double rawAngle = encoderAngle;
      final double rawAngle = turnEncoder.getAbsolutePosition();
      if (ENCODER_DEBUG_MODE) SmartDashboard.putNumber("raw ["+channel+"]", rawAngle);
      double degAngle = rawAngle * 360;
      if (ENCODER_DEBUG_MODE) SmartDashboard.putNumber("deg ["+channel+"]", degAngle);
      degAngle = degAngle - encoderOffsetDeg;
      if (ENCODER_DEBUG_MODE) SmartDashboard.putNumber("zeroed ["+channel+"]", degAngle);
      if (turnEncoderReversed) degAngle *= -1;
      if (ENCODER_DEBUG_MODE) SmartDashboard.putNumber("flipped ["+channel+"]", degAngle);
      degAngle = IEEEremainder(degAngle, 360);
      if (ENCODER_DEBUG_MODE) SmartDashboard.putNumber("final ["+channel+"]", degAngle);
      return degAngle;
    }

    /** reset all turn encoders to zero (untested) */
    public void resetEncoders() {
      turnEncoder.reset();
      // encoderAngle = 0;
    }

    /**
     * set module motors as described in a given state
     * @param desiredState the state describing the drive motor and turn motor outputs
     */
    public void setModuleState(SwerveModuleState desiredState) {
      if (PID_DEBUG_MODE) SmartDashboard.putString("swerve ["+channel+"] state", desiredState.toString());
      final double currentAngle = getTurnDeg();
      desiredState = optimize(desiredState, currentAngle);
      if (PID_DEBUG_MODE) SmartDashboard.putString("optimized ["+channel+"] state", desiredState.toString());
      final double angleError = desiredState.angle.getDegrees();
      double driveOutput = desiredState.speedMetersPerSecond / MAX_PHYSICAL_SPEED;
      double turnOutput  = KP_TURN * angleError;

      if (abs(driveOutput) < MIN_MOTOR_OUTPUT) driveOutput = NO_OUTPUT;
      if (abs(turnOutput)  < MIN_MOTOR_OUTPUT) turnOutput  = NO_OUTPUT;

      if (PID_DEBUG_MODE) {
        SmartDashboard.putNumber("drive ["+channel+"]", driveOutput);
        SmartDashboard.putNumber("turn ["+channel+"]", turnOutput);
        SmartDashboard.putNumber("encoder ["+channel+"]", currentAngle);
      }

      if (ENABLE_DRIVE_MOTORS) driveMotor.set(driveOutput);
      if (ENABLE_TURN_MOTORS)  turnMotor.set(turnOutput);
      // encoderAngle += turnOutput * encoderOutScalar;
    }

    /**
     * use a shortest path algorithm to calculate the minumum angle the wheel will have to spin
     * and whether or not the output to the drive motor should be inverted
     * @param desiredState unoptimized state describing target drive and turn motor outputs
     * @param currentAngle current degree angle of the turn motor
     * @return SwerveModuleState with optimized drive and turn motor outputs
     */
    private SwerveModuleState optimize(SwerveModuleState desiredState, double currentAngle) {

      final double targetAngle = desiredState.angle.getDegrees();
      if (PID_DEBUG_MODE) SmartDashboard.putNumber("targetAngle ["+channel+"]", targetAngle);
      final double rawError = targetAngle - currentAngle;
      if (PID_DEBUG_MODE) SmartDashboard.putNumber("rawError ["+channel+"]", rawError);
      final double optimizedError = IEEEremainder(rawError, 360);
      if (PID_DEBUG_MODE) SmartDashboard.putNumber("optimizedError ["+channel+"]", optimizedError);

      final double speed = desiredState.speedMetersPerSecond;
      final double optimizedSpeed;
      final double optimizedAngle;
      if (optimizedError < FLIP_THRESHOLD) {
        optimizedSpeed = speed;
        optimizedAngle = optimizedError;
      } else {
        optimizedSpeed = -speed;
        optimizedAngle = IEEEremainder(optimizedError, 180);
      }
      if (PID_DEBUG_MODE) SmartDashboard.putNumber("optimizedAngle ["+channel+"]", optimizedAngle);
      Rotation2d optimizedRotation = new Rotation2d(Units.degreesToRadians(optimizedAngle));
      return new SwerveModuleState(optimizedSpeed, optimizedRotation);
    }

    /** stop the drive and turn motors */
    public void stopMotors() {
      if (PID_DEBUG_MODE) {
        SmartDashboard.putNumber("drive ["+channel+"]", NO_OUTPUT);
        SmartDashboard.putNumber("turn ["+channel+"]", NO_OUTPUT);
      }
      driveMotor.set(NO_OUTPUT);
      turnMotor.set(NO_OUTPUT);
    }

    /** set drive and turn motors to brake mode */
    public void brakeMode() {
      driveMotor.setIdleMode(IdleMode.kBrake);
      turnMotor.setIdleMode(IdleMode.kBrake);
    }

    /** set drive and turn motors to caost mode */
    public void coastMode() {
      driveMotor.setIdleMode(IdleMode.kCoast);
      turnMotor.setIdleMode(IdleMode.kCoast);
    }

}
