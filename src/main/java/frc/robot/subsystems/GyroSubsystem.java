// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import static frc.robot.Constants.GyroConstants.*;
import static java.lang.Math.abs;

public class GyroSubsystem extends SubsystemBase {

  private final AHRS gyro = new AHRS(SPI.Port.kMXP);
  private double inclineOffset;

  /** contains robot gyro and all methods to directly interact with it */
  public GyroSubsystem() {
    // allow 1 sec for gyro to boot up. on a separate thread so other
    // functions can continue to run
    new Thread(() -> {
      try {
        Thread.sleep(1000);
        resetGyro();
        inclineOffset = gyro.getRoll();
      } catch (InterruptedException e) {
        e.printStackTrace();
        Thread.currentThread().interrupt();
      }
    }).start();
  }

  /**
   * @return current 2d angle of robot in interval (-180, 180]
   */
  public double getDegrees() {
    return Math.IEEEremainder(gyro.getAngle(), 360);
  }

  /**
   * @return angle of elevation of the robot
   */
  private double getIncline() {
    return inclineOffset - gyro.getRoll();
  }

  /**
   * @return true if robot is flat
   */
  public boolean isFlat() {
    return abs(getIncline()) < GYRO_TOLERANCE;
  }

  /**
   * @return true if robot is tilted upward
   */
  public boolean tiltedUp() {
    return getIncline() > FLAT + GYRO_TOLERANCE;
  }

  /**
   * @return true if robot is tilted downward
   */
  public boolean tiltedDown() {
    return getIncline() < FLAT - GYRO_TOLERANCE;
  }

  /**
   * resets robot gyro
   */
  public void resetGyro() {
    gyro.reset();
  }

  /**
   * @return current 2d angle of robot as a Rotation2d.
   * Angle is in interval (-180, 180] degrees or (-pi, pi] radians
   */
  public Rotation2d getGyroRotation2d() {
    return Rotation2d.fromDegrees(getDegrees());
  }

}
