package frc.robot.subsystems;

import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.kinematics.SwerveModuleState;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.SwerveModule;
import static frc.robot.Constants.DriveConstants.*;
import static frc.robot.Constants.ModuleConstants.*;
import static java.lang.Math.*;

public class SwerveSubsystem extends SubsystemBase {

  private final SwerveModule frontLeftModule  = new SwerveModule(
    FL_DRIVE_MOTOR_PORT,
    FL_TURN_MOTOR_PORT,
    FL_TURN_ENCODER_PORT,
    FL_ENCODER_OFFSET_DEG,
    FL_DRIVE_MOTOR_REVERSED,
    FL_TURN_MOTOR_REVERSED,
    FL_TURN_ENCODER_REVERSED
  );

  private final SwerveModule frontRightModule  = new SwerveModule(
    FR_DRIVE_MOTOR_PORT,
    FR_TURN_MOTOR_PORT,
    FR_TURN_ENCODER_PORT,
    FR_ENCODER_OFFSET_DEG,
    FR_DRIVE_MOTOR_REVERSED,
    FR_TURN_MOTOR_REVERSED,
    FR_TURN_ENCODER_REVERSED
  );

  private final SwerveModule backLeftModule  = new SwerveModule(
    BL_DRIVE_MOTOR_PORT,
    BL_TURN_MOTOR_PORT,
    BL_TURN_ENCODER_PORT,
    BL_ENCODER_OFFSET_DEG,
    BL_DRIVE_MOTOR_REVERSED,
    BL_TURN_MOTOR_REVERSED,
    BL_TURN_ENCODER_REVERSED
  );

  private final SwerveModule backRightModule  = new SwerveModule(
    BR_DRIVE_MOTOR_PORT,
    BR_TURN_MOTOR_PORT,
    BR_TURN_ENCODER_PORT,
    BR_ENCODER_OFFSET_DEG,
    BR_DRIVE_MOTOR_REVERSED,
    BR_TURN_MOTOR_REVERSED,
    BR_TURN_ENCODER_REVERSED
  );

  private final SwerveModule[] allSwerveModules = {
    frontLeftModule,
    frontRightModule,
    backLeftModule,
    backRightModule
  };

  /** put all swerve module motors in brake mode */
  public void brakeMode() {
    for(SwerveModule module : allSwerveModules) {
      module.brakeMode();
    }
   }

  /** put all swerve module motors in coast mode */
  public void coastMode(){
    for(SwerveModule module : allSwerveModules){
      module.coastMode();
    }
  }

  /** stop all swerve module motors */
  public void stopMotors(){
    for(SwerveModule module : allSwerveModules){
      module.stopMotors();
    }
  }

  /**
   * @param desiredMovement
   * set swerve module motors to the outputs needed to obtain the desired movement
   */
  public void setModules(ChassisSpeeds desiredMovement) {
    SwerveModuleState[] desiredStates = DRIVE_KINEMATICS.toSwerveModuleStates(desiredMovement);
    for (int i = 0; i < allSwerveModules.length; i++){
      SwerveModuleState desiredState = desiredStates[i];
      allSwerveModules[i].setModuleState(desiredState);
    }
  }

  /**
   * @param desiredMovement
   * set swerve module motors to the outputs needed to obtain the desired movement
   */
  public void setModules(SwerveModuleState[] desiredStates) {
    for (int i = 0; i < allSwerveModules.length; i++){
      SwerveModuleState desiredState = desiredStates[i];
      allSwerveModules[i].setModuleState(desiredState);
    }
  }

  /**
   * Checks if the current swerve modules closely enough match a set of desired states
   * @param desiredStates
   * Arrary of desired states to compare against
   * @return
   * true if the modules closely enough match the desired states, false if not
   */
  public boolean checkStates(SwerveModuleState[] desiredStates) {
    for (int i = 0; i < allSwerveModules.length; i++) {
      if (abs(allSwerveModules[i].getTurnDeg() - desiredStates[i].angle.getDegrees()) > ANGLE_TOLERANCE_THRESHOLD) {
        return false;
      }
    }
    return true;
  }
}
