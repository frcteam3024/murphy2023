package frc.robot.subsystems;

import static frc.robot.Constants.IntakeConstants.*;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;

import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class IntakeSubsystem extends SubsystemBase {
  
  private final VictorSPX leftIntakeMotor  = new VictorSPX(LEFT_INTAKE_ID);
  private final VictorSPX rightIntakeMotor = new VictorSPX(RIGHT_INTAKE_ID);

  /** contains intake motors and all methods to interact with them */
  public IntakeSubsystem() {
    leftIntakeMotor.setInverted(LEFT_MOTOR_INVERTED);
    rightIntakeMotor.setInverted(RIGHT_MOTOR_INVERTED);
  }

  /**
   * @param speed
   * Set intake motors to given speed
   */
  public void setOutput(double speed) {
    leftIntakeMotor.set(ControlMode.PercentOutput, speed);
    rightIntakeMotor.set(ControlMode.PercentOutput, speed);
  }

  /**
   * Set intake motors to brake mode
   */
  public void brakeMode() {
    leftIntakeMotor.setNeutralMode(NeutralMode.Brake);
    rightIntakeMotor.setNeutralMode(NeutralMode.Brake);
  }

  /**
   * Set intake motors to coast mode
   */
  public void coastMode() {
    leftIntakeMotor.setNeutralMode(NeutralMode.Coast);
    rightIntakeMotor.setNeutralMode(NeutralMode.Coast);
  }
}
