package frc.robot.subsystems;

import static frc.robot.Constants.ArmConstants.*;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;

// import edu.wpi.first.math.controller.PIDController;
// import edu.wpi.first.wpilibj.AnalogEncoder;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class ArmSubsystem extends SubsystemBase {
  
  private final TalonSRX armMotor;
  // private final AnalogEncoder armEncoder;
  // private final PIDController armPid;

  /** Contains arm motor and all methods to interact with it */
  public ArmSubsystem() {
    armMotor = new TalonSRX(ARM_MOTOR_ID);
    armMotor.setInverted(ARM_MOTOR_INVERTED);
    // armEncoder = new AnalogEncoder(ARM_ENCODER_PORT);
    // armPid = new PIDController(
    //   ARM_KP,
    //   ARM_KI,
    //   ARM_KD
    // );
  }

  // public double getArmAngle() {
  //   return armEncoder.getAbsolutePosition();
  // }


  /**
   * @param speed
   * Sets the arm motor to given speed
   */
  public void setArmMotorOutput(double speed) {
    armMotor.set(ControlMode.PercentOutput, speed);
  }

  /**
   * @param targetAngle
   * Moves arm to a desired angle.
   * Unusable because arm angle encoder was never mounted
   */
  // public void setArmMotorToAngle(double targetAngle) {
  //   double currentAngle = getArmAngle();
  //   double motorOut = armPid.calculate(currentAngle, targetAngle);
  //   setArmMotorOutput(motorOut);
  // }

  /**
   * Set arm motor to brake mode
   */
  public void brakeMode() {
    armMotor.setNeutralMode(NeutralMode.Brake);
  }

  /**
   * Set arm motor to coast mode
   */
  public void coastMode() {
    armMotor.setNeutralMode(NeutralMode.Coast);
  }
}
