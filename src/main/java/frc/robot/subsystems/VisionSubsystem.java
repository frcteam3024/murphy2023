package frc.robot.subsystems;

// import static frc.robot.Constants.FieldConstants.*;
import static frc.robot.Constants.VisionConstants.*;
import static java.lang.Math.*;

// import java.util.ArrayList;

import org.photonvision.PhotonCamera;
// import org.photonvision.PhotonPoseEstimator;
// import org.photonvision.PhotonPoseEstimator.PoseStrategy;
import org.photonvision.targeting.PhotonTrackedTarget;

// import edu.wpi.first.apriltag.AprilTag;
// import edu.wpi.first.apriltag.AprilTagFieldLayout;
// import edu.wpi.first.math.geometry.Rotation3d;
import edu.wpi.first.math.geometry.Transform3d;
import edu.wpi.first.math.geometry.Translation3d;
// import edu.wpi.first.wpilibj.DriverStation;

public class VisionSubsystem {

  private static final PhotonCamera camera = new PhotonCamera(CAMERA_NAME);
  // private final PhotonPoseEstimator cameraPoseEstimator;

  // private final AprilTag supplyTag;
  // private final AprilTag leftTag;
  // private final AprilTag centerTag;
  // private final AprilTag rightTag;

  public VisionSubsystem() {

    // if (DriverStation.getAlliance() == DriverStation.Alliance.Red) {
    //   supplyTag = new AprilTag(RED_SUPPLY_TAG_ID, RED_SUPPLY_TAG);
    //   leftTag   = new AprilTag(RED_LEFT_TAG_ID,   RED_LEFT_TAG  );
    //   centerTag = new AprilTag(RED_CENTER_TAG_ID, RED_CENTER_TAG);
    //   rightTag  = new AprilTag(RED_RIGHT_TAG_ID,  RED_RIGHT_TAG );
    // } else {  // DriverStation.Alliance.Blue
    //   supplyTag = new AprilTag(BLUE_SUPPLY_TAG_ID, BLUE_SUPPLY_TAG);
    //   leftTag   = new AprilTag(BLUE_LEFT_TAG_ID,   BLUE_LEFT_TAG  );
    //   centerTag = new AprilTag(BLUE_CENTER_TAG_ID, BLUE_CENTER_TAG);
    //   rightTag  = new AprilTag(BLUE_RIGHT_TAG_ID,  BLUE_RIGHT_TAG );
    // }
    
    // ArrayList<AprilTag> apriltagList = new ArrayList<>();
    // apriltagList.add(supplyTag);
    // apriltagList.add(leftTag  );
    // apriltagList.add(centerTag);
    // apriltagList.add(rightTag );

    // AprilTagFieldLayout aftl = 
    //  new AprilTagFieldLayout(apriltagList, FIELD_LENGTH, FIELD_WIDTH);

    // cameraPoseEstimator = new PhotonPoseEstimator(
    //    aftl, 
    //    PoseStrategy.CLOSEST_TO_REFERENCE_POSE, 
    //    camera,
    //    new Transform3d(ROBOT_TO_CAM, new Rotation3d())
    //  );

    // start out on apriltag pipeline
    camera.setPipelineIndex(APRILTAG_PIPELINE);

  }
  
  /**
   * get the id of "best" apriltag in sight
   * @return the apriltags id
   */
  public int apriltagID() {
    camera.setPipelineIndex(APRILTAG_PIPELINE);
    var result = camera.getLatestResult();
    PhotonTrackedTarget target = result.getBestTarget();
    return target.getFiducialId();
  }

  /**
   * Get the 3d distance from the robot to a desired node
   * @param nodeID the index of the desired node relative to an apriltag
   * @return a 3d vector with the x, y, and z offsets from the robot to the node
   */
  public Translation3d getVectorToNode(int nodeID) {
    camera.setPipelineIndex(APRILTAG_PIPELINE);
    var diff = cameraDiff(APRILTAG_PIPELINE);
    Translation3d camToObject = diff.getTranslation();
    return ROBOT_TO_CAM
      .plus(camToObject)
      .plus(getTagToNode(nodeID));
  }

  public static Transform3d cameraDiff(int pipe) {
    camera.setPipelineIndex(pipe);
    var result = camera.getLatestResult();
    PhotonTrackedTarget target = result.getBestTarget();
    return target.getBestCameraToTarget();
  }

  /**
   * gets the differance between the camera and the current apriltag on the x axis
   * @return diff on the x axis
   */
  public static double cameraDiffX(int pipe) {
    camera.setPipelineIndex(pipe);
    var diff = cameraDiff(pipe);
    return diff.getX();
  }

  /**
   * gets the differance between the camera and the current apriltag on the y axis
   * @return diff on the y axis
   */
  public static double cameraDiffY(int pipe) {
    camera.setPipelineIndex(pipe);
    var diff = cameraDiff(pipe);
    return diff.getY();
  }
  
  /**
   * gets the differance between the camera and the current apriltag on the z axis
   * @return diff on the z axis
   */
  public static double cameraDiffZ(int pipe) {
    camera.setPipelineIndex(pipe);
    var diff = cameraDiff(pipe);
    return diff.getZ();
  }

  public double distanceToObject(int pipe) {
    camera.setPipelineIndex(pipe);
    // change if the camera is not the origin
    var diff = cameraDiff(pipe);
    return hypot(diff.getX(), diff.getY());
  }

}