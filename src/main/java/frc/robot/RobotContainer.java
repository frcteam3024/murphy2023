// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import static frc.robot.Constants.OIConstants.*;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.wpilibj2.command.button.CommandXboxController;
import frc.robot.commands.ArmCommand;
import frc.robot.commands.AutoBalance;
import frc.robot.commands.AutoCubeBlanace;
import frc.robot.commands.AutoCubeTaxi;
import frc.robot.commands.GyroReset;
import frc.robot.commands.IntakeCommand;
import frc.robot.commands.LockWheels;
import frc.robot.commands.SwerveDriveCommand;
import frc.robot.subsystems.ArmSubsystem;
import frc.robot.subsystems.GyroSubsystem;
import frc.robot.subsystems.IntakeSubsystem;
import frc.robot.subsystems.SwerveSubsystem;

/**
 * This class is where the bulk of the robot should be declared. Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of the robot (including
 * subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
  // The robot's subsystems and commands are defined here...
  private final CommandXboxController copilotController;
  private final CommandXboxController driverController;

  private final SwerveSubsystem swerveSubsystem;
  private final GyroSubsystem gyroSubsystem;
  private final IntakeSubsystem intakeSubsystem;
  private final ArmSubsystem armSubsystem;
  
  private final SwerveDriveCommand swerveDriveCommand;
  private final ArmCommand armCommand;
  private final IntakeCommand intakeCommand;
  private final AutoBalance autoBalance;
  private final AutoCubeBlanace autoCubeBlanace;
  private final AutoCubeTaxi autoCubeTaxi;
  private final LockWheels lockWheels;
  private final GyroReset gyroReset;


  /** The container for the robot. Contains subsystems, OI devices, and commands. */
  public RobotContainer() {

    // initialize joysticks
    driverController  = new CommandXboxController(DRIVER_JOYSTICK_PORT);
    copilotController = new CommandXboxController(COPILOT_JOYSTICK_PORT);

    // initialize subsystems
    swerveSubsystem = new SwerveSubsystem();
    intakeSubsystem = new IntakeSubsystem();
    gyroSubsystem   = new GyroSubsystem();
    armSubsystem    = new ArmSubsystem();

    // initialize commands
    swerveDriveCommand = new SwerveDriveCommand(
        swerveSubsystem,
        gyroSubsystem,
        () ->  driverController.getLeftX(),
        () -> -driverController.getLeftY(),
        () ->  driverController.getRightX(),
        () ->  driverController.a().getAsBoolean()
      );

    intakeCommand = new IntakeCommand(
        intakeSubsystem, 
        () -> copilotController.getRightTriggerAxis()
            - copilotController.getLeftTriggerAxis()
    );

    armCommand = new ArmCommand(
        armSubsystem,
        () -> copilotController.getLeftY()
    );

    autoCubeTaxi = new AutoCubeTaxi(
        swerveSubsystem
    );

    autoBalance = new AutoBalance(
        swerveSubsystem,
        gyroSubsystem
    );

    autoCubeBlanace = new AutoCubeBlanace(
        swerveSubsystem,
        gyroSubsystem
    );

    lockWheels = new LockWheels(swerveSubsystem);
    gyroReset = new GyroReset(gyroSubsystem);

    // set commands to be automatically scheduled
    swerveSubsystem.setDefaultCommand(swerveDriveCommand);
    intakeSubsystem.setDefaultCommand(intakeCommand);
    armSubsystem.setDefaultCommand(armCommand);

    // Configure the button bindings
    configureButtonBindings();
  }

  /**
   * Use this method to define your button->command mappings. Buttons can be created by
   * instantiating a {@link GenericHID} or one of its subclasses ({@link
   * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing it to a {@link
   * edu.wpi.first.wpilibj2.command.button.JoystickButton}.
   */
  private void configureButtonBindings() {
    driverController.y().onTrue(gyroReset);
    driverController.x().onTrue(lockWheels);
  }

  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   */
  public CommandBase getNullAuto() {
    return null;
  }

  public CommandBase getCubeTaxiAuto() {
    return autoCubeTaxi;
  }

  public CommandBase getBalanceAuto() {
    return autoBalance;
  }

  public CommandBase getCubeBalanceAuto() {
    return autoCubeBlanace;
  }

}
